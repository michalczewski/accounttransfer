##REST API

| Method | url | Description |
| --- | --- | --- |
| **User:** |
| GET | /users | Get all user |
| GET | /users/{id} | Get user |
| PUT | /users/{id} | Update user |
| POST | /users | Add new user |
| DELETE | /users/{id} | Delete user |
| **Account:** |
| GET | /accounts | Get all accounts |
| GET | /accounts/{id} | Get account |
| PUT | /accounts | Update account |
| **Transfer:** |
| GET | /transfers | Get all transfers |
| GET | /transfers/{id} | Get transfer |
| POST | /transfers | Perform transfer |

Example of json send to perform transfer:
{fromUser:u1_id, toUser:u2_id, amount:money_amount, currency:currency_id}

##3rd party
- Server: Apache CXF
- JSON: Jackson
- Tests: jUnit, AssertJ, Apache HttpComponents
- DB: HSQLDB
- Logs: Log4j

##Execution

mvn exec:java
