package com.pm.account.model;

import org.codehaus.jackson.annotate.JsonProperty;

public class CurrencyAccount {

    public CurrencyAccount() {
    }

    public CurrencyAccount(long balance) {
        this(-1, balance);
    }

    public CurrencyAccount(long id, long balance) {
        this.id = id;
        this.balance = balance;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
           return false;
        CurrencyAccount other = (CurrencyAccount) obj;
        if (id != other.id)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return String.format("Account {id:[%d], balance: [%d]}", id, balance);
    }

    @JsonProperty
    private long id;

    @JsonProperty
    private long balance;
}
