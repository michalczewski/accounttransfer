package com.pm.account.model;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonProperty;

public class Transfer {

    public Transfer() {		
    }

    public Transfer(long accountFrom, long accountTo, long transferAmount, Date when) {
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.transferAmount = transferAmount;
        this.when = when;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getWhen() {
        return when;
    }

    public void setWhen(Date when) {
        this.when = when;
    }
    
    public long getAccountFrom() {
        return accountFrom;
    }

    public void setAccountFrom(long accountFrom) {
        this.accountFrom = accountFrom;
    }

    public long getAccountTo() {
        return accountTo;
    }

    public void setAccountTo(long accountTo) {
        this.accountTo = accountTo;
    }

    public long getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(long transferAmount) {
        this.transferAmount = transferAmount;
    }

    @JsonProperty
    private long id;	

    @JsonProperty
    private Date when;

    @JsonProperty
    private long accountFrom;

    @JsonProperty
    private long accountTo;

    @JsonProperty
    private long transferAmount;
}
