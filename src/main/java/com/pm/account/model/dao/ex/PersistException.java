package com.pm.account.model.dao.ex;

public class PersistException extends Exception {

    public PersistException(Throwable t) {
        super(t);
    }

    public PersistException(String msg) {
        super(msg);
    }
    
    private static final long serialVersionUID = 1L;
}
