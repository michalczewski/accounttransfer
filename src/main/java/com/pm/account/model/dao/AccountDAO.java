package com.pm.account.model.dao;

import com.pm.account.db.dao.ex.ActionException;
import com.pm.account.model.CurrencyAccount;
import com.pm.account.model.Transfer;
import com.pm.account.model.dao.ex.PersistException;

public interface AccountDAO extends DAO<CurrencyAccount> {

    void performTransfer(Transfer operation) throws PersistException, ActionException;
}
