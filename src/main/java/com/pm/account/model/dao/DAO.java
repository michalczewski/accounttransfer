package com.pm.account.model.dao;

import java.util.List;
import java.util.Optional;

import com.pm.account.model.dao.ex.PersistException;

public interface DAO<T> {

    Optional<T> get(long id) throws PersistException;

    List<T> getAll() throws PersistException;

    T insert(T t) throws PersistException;

    void update(T t) throws PersistException;

    void delete(T t) throws PersistException;
}
