package com.pm.account.model.dao;

import com.pm.account.model.User;

public interface UserDAO extends DAO<User> {
}
