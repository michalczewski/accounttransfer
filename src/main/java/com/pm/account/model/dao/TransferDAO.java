package com.pm.account.model.dao;

import java.util.List;
import java.util.Optional;

import com.pm.account.model.Transfer;
import com.pm.account.model.dao.ex.PersistException;

public interface TransferDAO {

    Optional<Transfer> get(long id) throws PersistException;

    List<Transfer> getAll() throws PersistException;

    Transfer insert(Transfer operation) throws PersistException;

    void delete(Transfer operation) throws PersistException;
}
