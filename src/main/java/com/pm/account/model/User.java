package com.pm.account.model;

import org.codehaus.jackson.annotate.JsonProperty;

public class User {

    public User() {
    }
    
    public User(String name, CurrencyAccount account) {
        this.name = name;
        this.account = account;		
    }

    public User(long id, String name, CurrencyAccount account) {
        this.id = id;
        this.name = name;
        this.account = account;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CurrencyAccount getAccount() {
        return account;
    }

    public void setAccount(CurrencyAccount account) {
        this.account = account;
    }

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (id != other.id)
            return false;
        return true;
    }

    public String toString() {
        return String.format("User {id: [%d], name:[%s], account:[%s]}", id, name, account);
    }

    @JsonProperty
    private long id;

    @JsonProperty
    private String name;

    @JsonProperty
    private CurrencyAccount account;
}
