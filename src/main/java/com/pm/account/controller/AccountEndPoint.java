package com.pm.account.controller;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import com.pm.account.controller.ex.MissingException;
import com.pm.account.db.dao.AccountDAOImpl;
import com.pm.account.model.CurrencyAccount;
import com.pm.account.model.dao.ex.PersistException;

@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
public class AccountEndPoint {

    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") long accountId) {
        LOG.info("Get Account with id " + accountId);
        ResponseBuilder responseBuilder = Response.serverError();
        try {
            CurrencyAccount account = getAccount(accountId);
            responseBuilder = Response.ok().entity(account);
        } catch (PersistException e) {
            LOG.error("Unable to get Account with id " + accountId, e);
            responseBuilder = Response.serverError().type(MediaType.TEXT_PLAIN_TYPE).entity(e.getMessage());
        } catch (MissingException e) {
            LOG.error("Missing Account with id " + accountId, e);
            responseBuilder = Response.status(Status.NOT_FOUND).entity(ACCOUNT_NOT_FOUND);
        }
        return responseBuilder.build();
    }

    @GET
    public Response getAll() {
        LOG.info("Get All Account");
        ResponseBuilder responseBuilder = Response.serverError();
        try {
            List<CurrencyAccount> accounts = new AccountDAOImpl().getAll();
            responseBuilder = Response.ok().entity(accounts);
        } catch (PersistException e) {
            LOG.error("Unable to get all Accounts", e);
            responseBuilder = Response.serverError().type(MediaType.TEXT_PLAIN_TYPE).entity(e.getMessage());
        }
        return responseBuilder.build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(CurrencyAccount account) {
        LOG.info("Update Account with id " + account.getId());
        ResponseBuilder responseBuilder = Response.serverError();
        try {
            if (account.getBalance() >= 0) {
                //check if user exists
                getAccount(account.getId());
                new AccountDAOImpl().update(account);
                responseBuilder = Response.ok().entity(account);
            } else {
                LOG.error("Negative balance");
                responseBuilder = Response.status(Status.BAD_REQUEST).entity("Balance can not be negative");
            }
        } catch (MissingException e) {
            LOG.error("Missing account for update", e);
            responseBuilder = Response.status(Status.NOT_FOUND).entity(ACCOUNT_NOT_FOUND);
        } catch (PersistException e) {
            LOG.error("Unable to update Account with id " + account.getId(), e);
            responseBuilder = Response.serverError().type(MediaType.TEXT_PLAIN_TYPE).entity(e.getMessage());
        }
        return responseBuilder.build();
    }

    private CurrencyAccount getAccount(long id) throws PersistException, MissingException {
        Optional<CurrencyAccount> account = new AccountDAOImpl().get(id);
        return account.orElseThrow(() -> new MissingException());
    }
    
    private final static Logger LOG = Logger.getLogger(AccountEndPoint.class);
    private static final String ACCOUNT_NOT_FOUND = "Account not found";
}
