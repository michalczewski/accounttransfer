package com.pm.account.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import com.pm.account.controller.ex.MissingException;
import com.pm.account.db.dao.AccountDAOImpl;
import com.pm.account.db.dao.TransferDAOImpl;
import com.pm.account.db.dao.ex.ActionException;
import com.pm.account.model.Transfer;
import com.pm.account.model.dao.ex.PersistException;

@Path("/transfers")
@Produces(MediaType.APPLICATION_JSON)
public class TransferEndPoint {

    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") long transferId) {
        LOG.info("Get Transfer with id " + transferId);
        ResponseBuilder responseBuilder = Response.serverError();
        try {
            Transfer transfer = getOperation(transferId);
            responseBuilder = Response.ok().entity(transfer);
        } catch (PersistException e) {
            LOG.error("Unable to get Transfer with id " + transferId, e);
            responseBuilder = Response.serverError().type(MediaType.TEXT_PLAIN_TYPE).entity(e.getMessage());
        } catch (MissingException e) {
            LOG.error("Missing Transfer with id " + transferId, e);
            responseBuilder = Response.status(Status.NOT_FOUND).entity(TRANSFER_NOT_FOUND);
        }
        return responseBuilder.build();
    }

    @GET
    public Response getAll() {
        LOG.info("Get all Transfers");
        ResponseBuilder responseBuilder = Response.serverError();
        try {
            List<Transfer> transfers = new TransferDAOImpl().getAll();
            responseBuilder = Response.ok().entity(transfers);
        } catch (PersistException e) {
            LOG.error("Unable to get all Transfers", e);
            responseBuilder = Response.serverError().entity(e.getMessage());
        }
        return responseBuilder.build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response transfer(Transfer transfer) {
        LOG.info("New Transfer");
        ResponseBuilder responseBuilder = Response.serverError();
        try {
            if (transfer.getTransferAmount() >= 0) {
                transfer.setWhen(new Date());
                new AccountDAOImpl().performTransfer(transfer);
                responseBuilder = Response.ok();
            } else {
                responseBuilder = Response.status(Status.BAD_REQUEST).entity("Transfer amount can not be negative");
            }
        } catch (ActionException e) {
            LOG.error("Unable to perform Transfer", e);
            responseBuilder = Response.status(Status.BAD_REQUEST).entity(e.getMessage());             
        } catch (PersistException e) {
            LOG.error("Unable to perform Transfer", e);
            responseBuilder = Response.serverError().entity(e.getMessage());
        }
        return responseBuilder.build();
    }

    private Transfer getOperation(long id) throws PersistException, MissingException {
        Optional<Transfer> operation = new TransferDAOImpl().get(id);
        return operation.orElseThrow(() -> new MissingException());
    }
    
    private final static Logger LOG = Logger.getLogger(TransferEndPoint.class);
    private static final String TRANSFER_NOT_FOUND = "Transfer not found";
}
