package com.pm.account.controller;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import com.pm.account.controller.ex.MissingException;
import com.pm.account.db.dao.UserDAOImpl;
import com.pm.account.model.CurrencyAccount;
import com.pm.account.model.User;
import com.pm.account.model.dao.ex.PersistException;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
public class UserEndPoint {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response add(User user) {
        LOG.info("Create new User");
        ResponseBuilder responseBuilder = Response.serverError();
        try {
            user = addUser(user);
            responseBuilder = Response.status(Status.CREATED).entity(user);
        } catch (PersistException e) {
            LOG.error("Unable to create User", e);
            responseBuilder = Response.serverError().type(MediaType.TEXT_PLAIN_TYPE).entity(e.getMessage());
        }
        return responseBuilder.build();
    }

    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") long userId) {
        LOG.info("Get User with id " + userId);
        ResponseBuilder responseBuilder = Response.serverError();
        try {
            User user = getUser(userId);
            responseBuilder = Response.ok().entity(user);
        } catch (PersistException e) {
            LOG.error("Unable to get User with id " + userId, e);
            responseBuilder = Response.serverError().type(MediaType.TEXT_PLAIN_TYPE).entity(e.getMessage());
        } catch (MissingException e) {
            LOG.error("Missing User with id " + userId, e);
            responseBuilder = Response.status(Status.NOT_FOUND).entity(USER_NOT_FOUND);
        }
        return responseBuilder.build();
    }

    @GET
    public Response getAll() {
        LOG.info("Get all Users");
        ResponseBuilder responseBuilder = Response.serverError();
        try {
            List<User> users = new UserDAOImpl().getAll();
            responseBuilder = Response.ok().entity(users);
        } catch (PersistException e) {
            LOG.error("Unable to get all Users", e);
            responseBuilder = Response.serverError().type(MediaType.TEXT_PLAIN_TYPE).entity(e.getMessage());
        }
        return responseBuilder.build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(User user) {
        LOG.info("Get User with id " + user.getId());
        ResponseBuilder responseBuilder = Response.serverError();
        try {
            //check if user exists
            getUser(user.getId());
            new UserDAOImpl().update(user);
            responseBuilder = Response.ok().entity(user);
        } catch (MissingException e) {
            LOG.error("Missing User with id " + user.getId(), e);
            responseBuilder = Response.status(Status.NOT_FOUND).entity(USER_NOT_FOUND);
        } catch (PersistException e) {
            LOG.error("Unable to update User with id " + user.getId(), e);
            responseBuilder = Response.serverError().type(MediaType.TEXT_PLAIN_TYPE).entity(e.getMessage());
        }
        return responseBuilder.build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") long userId) {
        LOG.info("Delete User with id " + userId);
        ResponseBuilder responseBuilder = Response.serverError();
        try {
            User user = getUser(userId);
            new UserDAOImpl().delete(user);
            responseBuilder = Response.ok();
        } catch (MissingException e) {
            LOG.error("Missing User with id " + userId, e);
            responseBuilder = Response.status(Status.NOT_FOUND).entity(USER_NOT_FOUND);
        } catch (PersistException e) {
            LOG.error("Unable to delete User with id " + userId, e);
            responseBuilder = Response.serverError().type(MediaType.TEXT_PLAIN_TYPE).entity(e.getMessage());
        }
        return responseBuilder.build();
    }

    private User addUser(User user) throws PersistException {
        if (user.getAccount() == null) {
            user.setAccount(new CurrencyAccount());
        }
        user = new UserDAOImpl().insert(user);
        return user;
    }

    private User getUser(long id) throws PersistException, MissingException {
        Optional<User> user = new UserDAOImpl().get(id);
        return user.orElseThrow(() -> new MissingException());
    }

    private final static Logger LOG = Logger.getLogger(UserEndPoint.class);
    private final static String USER_NOT_FOUND = "User not found";
}
