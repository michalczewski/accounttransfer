package com.pm.account;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.BusFactory;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.hsqldb.cmdline.SqlToolError;

import com.pm.account.controller.AccountEndPoint;
import com.pm.account.controller.TransferEndPoint;
import com.pm.account.controller.UserEndPoint;
import com.pm.account.db.DBFactory;

public class MainServer {

    public static void main(String ...strings) throws GeneralSecurityException, IOException, SqlToolError, SQLException {
        new MainServer();
    }

    public MainServer() throws GeneralSecurityException, IOException, SqlToolError, SQLException {
        DBFactory.createDB();
        fillDB();
        JAXRSServerFactoryBean factoryBean = new JAXRSServerFactoryBean();
        factoryBean.setResourceProvider(new SingletonResourceProvider(new AccountEndPoint()));
        factoryBean.setResourceProvider(new SingletonResourceProvider(new UserEndPoint()));
        factoryBean.setResourceProvider(new SingletonResourceProvider(new TransferEndPoint()));
        factoryBean.setAddress("http://localhost:8089");
        List<Object> providers = new ArrayList<Object>();
        providers.add(new JacksonJsonProvider());
        factoryBean.setProviders(providers);
        Server server = factoryBean.create();
        System.out.println("Press Enter to close server");		
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        server.stop();
        server.destroy();
        BusFactory.getDefaultBus().shutdown(true);
    }
    
    private void fillDB() throws SQLException {
        try (Connection connection = DBFactory.getConnection()) {
            connection.prepareStatement("insert into accounts(account_id, balance) values(1, 10000)").execute();
            connection.prepareStatement("insert into accounts(account_id, balance) values(2, 20000)").execute();
            connection.prepareStatement("insert into accounts(account_id, balance) values(3, 30000)").execute();
            connection.prepareStatement("insert into users(user_id, name, account_id) values(1, 'Frodo', 1)").execute();
            connection.prepareStatement("insert into users(user_id, name, account_id) values(2, 'Sam', 2)").execute();
            connection.prepareStatement("insert into users(user_id, name, account_id) values(3, 'Gandalf', 3)").execute();
        }
    }
}
