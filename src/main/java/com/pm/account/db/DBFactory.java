package com.pm.account.db;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.apache.log4j.Logger;
import org.hsqldb.cmdline.SqlFile;
import org.hsqldb.cmdline.SqlToolError;

import com.pm.account.db.dao.ex.ActionException;
import com.pm.account.db.dao.func.ObjectCreator;
import com.pm.account.db.dao.func.PrepareStatement;
import com.pm.account.db.dao.func.SqlAction;

public class DBFactory {

    private final static Logger LOG = Logger.getLogger(DBFactory.class);
    static {
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
        } catch (ClassNotFoundException e) {
            LOG.fatal("Unable to load HSQLDB Driver", e);
        }
    }

    public static void createDB() throws SQLException, IOException, SqlToolError {
        try (
              Connection connection = getConnection();
              InputStream inputStream = DBFactory.class.getResourceAsStream("/create_db.sql")
        ) {
            SqlFile sqlFile = new SqlFile(new InputStreamReader(inputStream), "init", System.out, "UTF-8", false, new File("."));
            sqlFile.setConnection(connection);
            sqlFile.execute();
        }
    }

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:hsqldb:mem:mymemdb", "SA", "");
    }

    public static <T> Optional<T> singleSelect(String query, PrepareStatement statementPrepare, ObjectCreator<T> converter) throws SQLException {
        T dbRecord = null;
        try (Connection connection = DBFactory.getConnection(); 
                PreparedStatement statement = connection.prepareStatement(query)) {
            statementPrepare.prepare(statement);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                dbRecord = converter.create(rs);
            }
        }
        return Optional.ofNullable(dbRecord);
    }

    public static <T> List<T> multiSelect(String query, ObjectCreator<T> converter) throws SQLException {
        return multiSelect(query, ps->{}, converter);
    }

    public static <T> List<T> multiSelect(String query, PrepareStatement statementPrepare, ObjectCreator<T> converter) throws SQLException {
        List<T> dbRecords = new ArrayList<>();
        try (Connection connection = DBFactory.getConnection(); 
                PreparedStatement statement = connection.prepareStatement(query)) {
            statementPrepare.prepare(statement);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
               dbRecords.add(converter.create(rs));
            }
        }
        return dbRecords;
    }

    public static void performTransaction(SqlAction ...actions) throws SQLException, ActionException {
        try (Connection connection = DBFactory.getConnection()) {
            connection.setAutoCommit(false);
            try {
                for (SqlAction action : actions) {
                    action.performAction(connection);
                }
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw e;
            }
        }
    }

    public static <T> SqlAction createInsertAction(String query, PrepareStatement statementPrepare, Consumer<Long> idConsumer) {
        return connection -> {
            LOG.trace("Perform insert action: " + query);
            try (PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
                statementPrepare.prepare(statement);
                if (statement.executeUpdate() > 0) {
                    ResultSet rs = statement.getGeneratedKeys();
                    if (rs.next()) {
                        idConsumer.accept(rs.getLong(1));
                    }
                }
            }
        };
    }

    public static <T> SqlAction createUpdateAction(String query, PrepareStatement statementPrepare) {
        return connection -> {
            LOG.trace("Perform update action: " + query);
            try (PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
                statementPrepare.prepare(statement);
                statement.executeUpdate();
            }
        };
    }

    public static <T> SqlAction createDeleteAction(String query, PrepareStatement statementPrepare) {
        return connection -> {
            LOG.trace("Perform delete action: " + query);
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                statementPrepare.prepare(statement);
                statement.execute();
            }
        };
    }
}
