package com.pm.account.db.dao.func;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * This interface should be used to set parameters in passed statement.
 * 
 * @author Pawel Michalczewski
 *
 */
@FunctionalInterface
public interface PrepareStatement {

    public void prepare(PreparedStatement ps) throws SQLException;
}
