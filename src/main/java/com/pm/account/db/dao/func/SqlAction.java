package com.pm.account.db.dao.func;

import java.sql.Connection;
import java.sql.SQLException;

import com.pm.account.db.dao.ex.ActionException;

/**
 * This interface should be used to perform sql action(s) which will be part of one transaction
 * 
 * @author Pawel Michalczewski
 *
 */
@FunctionalInterface
public interface SqlAction {

    void performAction(Connection connection) throws SQLException, ActionException;
}
