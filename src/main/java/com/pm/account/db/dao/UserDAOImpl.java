package com.pm.account.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import com.pm.account.db.DBFactory;
import com.pm.account.db.dao.ex.ActionException;
import com.pm.account.db.dao.func.SqlAction;
import com.pm.account.model.CurrencyAccount;
import com.pm.account.model.User;
import com.pm.account.model.dao.UserDAO;
import com.pm.account.model.dao.ex.PersistException;

public class UserDAOImpl implements UserDAO {

    @Override
    public Optional<User> get(long userId) throws PersistException {
        try {
            return DBFactory.singleSelect("select * from users u inner join accounts a on u.user_id=a.account_id where user_id=?", 
                    prepareStatement -> prepareStatement.setLong(1, userId), 
                    rs -> load(rs));
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    @Override
    public List<User> getAll() throws PersistException {
        try {
            return DBFactory.multiSelect("select * from users u inner join accounts a on u.user_id=a.account_id", 
                    rs -> load(rs));
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    @Override
    public User insert(User user) throws PersistException {
        SqlAction insertAccount = new AccountDAOImpl().createInsertAction(user.getAccount());
        SqlAction insertUser = createInsertAction(user);
        try {
            DBFactory.performTransaction(insertAccount, insertUser);
            return user;
        } catch (SQLException | ActionException e) {
            throw new PersistException(e);
        }
    }

    @Override
    public void update(User user) throws PersistException {
        SqlAction updateAccount = new AccountDAOImpl().createUpdateAction(user.getAccount());
        SqlAction updateUser = createUpdateAction(user);
        try {
            DBFactory.performTransaction(updateAccount, updateUser);
        } catch (SQLException | ActionException e) {
            throw new PersistException(e);
        }
    }

    @Override
    public void delete(User user) throws PersistException {
        SqlAction deleteUser = createDeleteAction(user);
        SqlAction deleteAccount = new AccountDAOImpl().createDeleteAction(user.getAccount());
        try {
            DBFactory.performTransaction(deleteUser, deleteAccount);
        } catch (SQLException | ActionException e) {
            throw new PersistException(e);
        }
    }

    SqlAction createInsertAction(User user) {
        return DBFactory.createInsertAction("insert into users(name, account_id) values(?,?)", 
                ps -> {
                        ps.setString(1, user.getName()); 
                        ps.setLong(2, user.getAccount().getId());
                }, id -> user.setId(id));
    }

    SqlAction createUpdateAction(User user) {
        return DBFactory.createUpdateAction("update users set name=? where user_id=?", 
                ps -> {
                    ps.setString(1, user.getName()); 
                    ps.setLong(2, user.getId());
                });
    }

    SqlAction createDeleteAction(User user) {
        return DBFactory.createDeleteAction("delete from users where user_id=?", 
                ps -> ps.setLong(1, user.getId()));
    }
    
    User load(ResultSet resultSet) throws SQLException {
        CurrencyAccount account = new AccountDAOImpl().load(resultSet);		
        User user = new User();
        user.setId(resultSet.getLong("user_id"));
        user.setName(resultSet.getString("name"));	
        user.setAccount(account);
        return user;
    }
}
