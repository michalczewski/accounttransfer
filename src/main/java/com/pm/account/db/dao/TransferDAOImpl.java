package com.pm.account.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.pm.account.db.DBFactory;
import com.pm.account.db.dao.ex.ActionException;
import com.pm.account.db.dao.func.SqlAction;
import com.pm.account.model.Transfer;
import com.pm.account.model.dao.TransferDAO;
import com.pm.account.model.dao.ex.PersistException;

public class TransferDAOImpl implements TransferDAO {

    @Override
    public Optional<Transfer> get(long transferId) throws PersistException {
        try {
            return DBFactory.singleSelect("select * from operations where operation_id=?", 
                    ps -> ps.setLong(1, transferId), 
                    rs -> load(rs));
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    @Override
    public List<Transfer> getAll() throws PersistException {
        try {
            return DBFactory.multiSelect("select * from operations", 
                    rs -> load(rs));
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    @Override
    public Transfer insert(Transfer transfer) throws PersistException {
        try {
            DBFactory.performTransaction(createInsertAction(transfer));
            return transfer;
        } catch (SQLException | ActionException e) {
            throw new PersistException(e);
        }
    }

    @Override
    public void delete(Transfer transfer) throws PersistException {
        try {
            DBFactory.performTransaction(createDeleteAction(transfer));
        } catch (SQLException | ActionException e) {
            throw new PersistException(e);
        }
    }

    SqlAction createInsertAction(Transfer transfer) {
        return DBFactory.createInsertAction("insert into operations(when_time, account_from, account_to, transfer_amount) values(?, ?, ?, ?)", 
                ps -> {
                    ps.setTimestamp(1, new Timestamp(transfer.getWhen().getTime()));
                    ps.setLong(2, transfer.getAccountFrom());
                    ps.setLong(3, transfer.getAccountTo());
                    ps.setLong(4, transfer.getTransferAmount());
                }, 
                id -> transfer.setId(id));
	   }

    SqlAction createDeleteAction(Transfer transfer) {
        return DBFactory.createDeleteAction("delete from operations where operation_id=?", 
                ps -> ps.setLong(1, transfer.getId()));
    }

    Transfer load(ResultSet resultSet) throws SQLException {
        Transfer transfer = new Transfer();
        Timestamp timestamp = resultSet.getTimestamp("when_time");
        transfer.setWhen(new Date(timestamp.getTime()));
        transfer.setAccountFrom(resultSet.getLong("account_from"));
        transfer.setAccountTo(resultSet.getLong("account_to"));
        transfer.setTransferAmount(resultSet.getLong("transfer_amount"));
        return transfer;
    }
}
