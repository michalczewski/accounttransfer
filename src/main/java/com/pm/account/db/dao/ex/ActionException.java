package com.pm.account.db.dao.ex;

public class ActionException extends Exception {

    public ActionException(String msg) {
        super(msg);
    }

    private static final long serialVersionUID = 1L;
}
