package com.pm.account.db.dao.func;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * This interface should be used to create object instance from db result set
 * 
 * @author Pawel Michalczewski
 *
 * @param <T> type of created object instance
 */
@FunctionalInterface
public interface ObjectCreator<T> {

    public T create(ResultSet rs) throws SQLException;
}
