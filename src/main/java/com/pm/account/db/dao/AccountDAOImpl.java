package com.pm.account.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;

import com.pm.account.db.DBFactory;
import com.pm.account.db.dao.ex.ActionException;
import com.pm.account.db.dao.func.SqlAction;
import com.pm.account.model.CurrencyAccount;
import com.pm.account.model.Transfer;
import com.pm.account.model.dao.AccountDAO;
import com.pm.account.model.dao.ex.PersistException;

public class AccountDAOImpl implements AccountDAO {

    @Override
    public Optional<CurrencyAccount> get(long accountId) throws PersistException {
        try {
            return DBFactory.singleSelect("select * from accounts where account_id=?", 
                    ps -> ps.setLong(1, accountId), 
                    rs -> load(rs));
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    @Override
    public List<CurrencyAccount> getAll() throws PersistException {
        try {
            return DBFactory.multiSelect("select * from accounts", 
                    rs -> load(rs));
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    @Override
    public CurrencyAccount insert(CurrencyAccount account) throws PersistException {
        try {
            DBFactory.performTransaction(createInsertAction(account));
            return account;
        } catch (SQLException | ActionException e) {
            throw new PersistException(e);
        }
    }

    @Override
    public void update(CurrencyAccount account) throws PersistException {
        try {
            if (account.getBalance() < 0) {
                throw new PersistException("Balance can not be negative");
            }
            DBFactory.performTransaction(createUpdateAction(account));
        } catch (SQLException | ActionException e) {
            throw new PersistException(e);
        }
    }

    @Override
    public void delete(CurrencyAccount account) throws PersistException {
        try {
            DBFactory.performTransaction(createDeleteAction(account));
        } catch (SQLException | ActionException e) {
            throw new PersistException(e);
        }
    }

    @Override
    public void performTransfer(Transfer operation) throws ActionException, PersistException {
        try {
            DBFactory.performTransaction(createTransferAction(operation), 
                    new TransferDAOImpl().createInsertAction(operation));
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    CurrencyAccount load(ResultSet resultSet) throws SQLException {
        CurrencyAccount account = new CurrencyAccount();
        account.setId(resultSet.getLong("account_id"));
        account.setBalance(resultSet.getLong("balance"));
        return account;
    }

    SqlAction createTransferAction(Transfer transfer) {
        return connection -> {
            LOG.debug("Perform transfer");
            long balanceFrom = getBalanceWithRecordLock(connection, transfer.getAccountFrom());
            if (balanceFrom < transfer.getTransferAmount()) {
                throw new ActionException("Insufficient founds");
            }
            long balanceTo = getBalanceWithRecordLock(connection, transfer.getAccountTo());
            
            PreparedStatement statement = connection.prepareStatement("update accounts set balance=? where account_id=?");
            statement.setLong(1, balanceFrom - transfer.getTransferAmount());
            statement.setLong(2, transfer.getAccountFrom());
            statement.addBatch();
            statement.setLong(1, balanceTo + transfer.getTransferAmount());
            statement.setLong(2, transfer.getAccountTo());
            statement.addBatch();
            statement.executeBatch();
        };
    }

    SqlAction createInsertAction(CurrencyAccount account) {
        return DBFactory.createInsertAction("insert into accounts(balance) values(?)", 
                ps -> ps.setLong(1, account.getBalance()), 
                id -> account.setId(id));
    }

    SqlAction createUpdateAction(CurrencyAccount account) {
        return DBFactory.createUpdateAction("update accounts set balance=? where account_id=?", 
                ps -> {
                    ps.setLong(1, account.getBalance());
                    ps.setLong(2, account.getId());
                });
    }

    SqlAction createDeleteAction(CurrencyAccount account) {
        return DBFactory.createDeleteAction("delete from accounts where account_id=?", 
                ps -> ps.setLong(1, account.getId()));
    }
    
    private long getBalanceWithRecordLock(Connection connection, long accountId) throws ActionException, SQLException {
        PreparedStatement statement = connection.prepareStatement("select balance from accounts where account_id=? for update");
        statement.setLong(1, accountId);
        ResultSet rs = statement.executeQuery();
        if (rs.next()) {
            return rs.getLong(1);
        } else {
            throw new ActionException("Account is missing");
        }
    }

    private final static Logger LOG = Logger.getLogger(AccountDAOImpl.class);
}
