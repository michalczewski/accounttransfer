package com.pm.account.model.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import org.hsqldb.cmdline.SqlToolError;
import org.junit.jupiter.api.Test;

import com.pm.account.db.dao.UserDAOImpl;
import com.pm.account.model.CurrencyAccount;
import com.pm.account.model.User;
import com.pm.account.model.dao.ex.PersistException;
import com.pm.account.utils.DAOTest;

public class UserDAOTest extends DAOTest {

	@Test
	public void getUser() throws SqlToolError, SQLException, IOException, PersistException {
		Optional<User> user = new UserDAOImpl().get(0);
		assertThat(user.isPresent()).isFalse();
		
		user = new UserDAOImpl().get(1);
		assertThat(user.isPresent()).isTrue();
		assertThat(user.get()).isEqualToComparingFieldByFieldRecursively(user1);
	}
	
	@Test
	public void crudTest() throws PersistException {
		CurrencyAccount account = new CurrencyAccount();
		account.setBalance(123);
		User user = new User();
		user.setName("Sauron");
		user.setAccount(account);

		//Create
		UserDAOImpl impl = new UserDAOImpl();
		impl.insert(user);
		assertThat(user.getId()).isEqualTo(4);
		
		//Read
		assertThat(impl.get(4).get()).isEqualToComparingFieldByField(user);
		
		//Update
		user.setName("Saruman");
		impl.update(user);
		assertThat(impl.get(4).get()).isEqualToComparingFieldByField(user);
		
		//Delete
		impl.delete(user);
		assertThat(impl.get(4).isPresent()).isFalse();
	}
	
	@Test
	public void failedDelete() throws PersistException {
		UserDAOImpl dao = new UserDAOImpl(); 
		User user = dao.get(3).get();
		user.getAccount().setId(1);
		
		assertThatExceptionOfType(PersistException.class).isThrownBy(() -> dao.delete(user));		
		assertThat(dao.get(3).isPresent()).isTrue();
	}
	
}
