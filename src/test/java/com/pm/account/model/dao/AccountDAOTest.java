package com.pm.account.model.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.jupiter.api.Test;

import com.pm.account.db.dao.AccountDAOImpl;
import com.pm.account.db.dao.TransferDAOImpl;
import com.pm.account.db.dao.ex.ActionException;
import com.pm.account.model.Transfer;
import com.pm.account.model.dao.ex.PersistException;
import com.pm.account.utils.DAOTest;

public class AccountDAOTest extends DAOTest {
	@Test
	public void moneyTransaction() throws PersistException {
		Transfer operation = new Transfer(account1.getId(), account2.getId(), 10, new Date());
		AccountDAOImpl dao = new AccountDAOImpl();
		try {
			dao.performTransfer(operation);
		} catch (ActionException e) {
			e.printStackTrace();
		}
		assertThat(dao.get(1).get().getBalance()).isEqualTo(account1.getBalance() - 10);
		assertThat(dao.get(2).get().getBalance()).isEqualTo(account2.getBalance() + 10);
	}
	
	@Test
	public void aLotOfTransactions() throws PersistException {
		AccountDAOImpl dao = new AccountDAOImpl();
		CountDownLatch latch = new CountDownLatch(3);
		ExecutorService executor = Executors.newFixedThreadPool(3);
		executor.execute(() -> {
			Transfer operation = new Transfer(account1.getId(), account2.getId(), 1, new Date());
			for (int i=0; i<1000; ++i) {				
				try {
					dao.performTransfer(operation);
				} catch (ActionException | PersistException e) {
					e.printStackTrace();
				}
			}
			latch.countDown();
		});
		executor.execute(() -> {
			Transfer operation = new Transfer(account2.getId(), account3.getId(), 1, new Date());
			for (int i=0; i<1000; ++i) {
				try {
					dao.performTransfer(operation);
				} catch (ActionException | PersistException e) {
					e.printStackTrace();
				}
			}
			latch.countDown();
		});
		executor.execute(() -> {
			Transfer operation = new Transfer(account3.getId(), account1.getId(), 1, new Date());
			for (int i=0; i<1000; ++i) {
				try {
					dao.performTransfer(operation);
				} catch (ActionException | PersistException e) {
					e.printStackTrace();
				}
			}
			latch.countDown();
		});

		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertThat(dao.get(1).get().getBalance()).isEqualTo(account1.getBalance());
		assertThat(dao.get(2).get().getBalance()).isEqualTo(account2.getBalance());
		assertThat(dao.get(3).get().getBalance()).isEqualTo(account3.getBalance());
	}
	
	@Test
	public void insufficientFounds() throws PersistException {
		Transfer operation = new Transfer(account1.getId(), account2.getId(), account1.getBalance() + 1, new Date());
		assertThatExceptionOfType(ActionException.class).isThrownBy(() -> 
			new AccountDAOImpl().performTransfer(operation));
		assertThat(new AccountDAOImpl().get(1).get().getBalance()).isEqualTo(account1.getBalance());
		assertThat(new AccountDAOImpl().get(2).get().getBalance()).isEqualTo(account2.getBalance());
		assertThat(new TransferDAOImpl().getAll()).isEmpty();
	}
	
	@Test
	public void allMoneyTransfer() throws ActionException, PersistException {
		Transfer operation = new Transfer(account1.getId(), account2.getId(), account1.getBalance(), new Date());
		new AccountDAOImpl().performTransfer(operation);
		assertThat(new AccountDAOImpl().get(1).get().getBalance()).isEqualTo(0);
		assertThat(new AccountDAOImpl().get(2).get().getBalance()).isEqualTo(account1.getBalance() + account2.getBalance());
		assertThat(new TransferDAOImpl().getAll().get(0)).isEqualToComparingOnlyGivenFields(operation, "accountFrom", "accountTo", "transferAmount");
	}
	
	@Test
	public void setIncorrectBalance() {
		account1.setBalance(-10);
		assertThatExceptionOfType(PersistException.class).isThrownBy(() -> {new AccountDAOImpl().update(account1);});
	}
}
