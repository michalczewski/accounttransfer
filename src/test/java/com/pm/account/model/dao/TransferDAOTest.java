package com.pm.account.model.dao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import org.junit.jupiter.api.Test;

import com.pm.account.db.dao.TransferDAOImpl;
import com.pm.account.model.Transfer;
import com.pm.account.model.dao.ex.PersistException;
import com.pm.account.utils.DAOTest;

public class TransferDAOTest extends DAOTest {

    @Test
    public void crudTest() throws PersistException {
        Transfer transfer = new Transfer();
        transfer.setAccountFrom(account1.getId());
        transfer.setAccountTo(account2.getId());
        transfer.setTransferAmount(account1.getBalance());
        transfer.setWhen(new Date());
        
        TransferDAOImpl impl = new TransferDAOImpl();
        impl.insert(transfer);
        assertThat(transfer.getId()).isEqualTo(0); 
        assertThat(impl.get(0).get()).isEqualToComparingFieldByField(transfer);
                
        //Delete
        impl.delete(transfer);
        assertThat(impl.get(0).isPresent()).isFalse();
    }
}
