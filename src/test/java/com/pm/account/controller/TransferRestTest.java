package com.pm.account.controller;

import static org.assertj.core.api.Assertions.assertThat;
import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.jupiter.api.Test;

import com.pm.account.model.Transfer;
import com.pm.account.utils.HttpTest;
import com.pm.account.utils.RestTest;

public class TransferRestTest extends RestTest {

    @Test
    public void transfers() throws JsonGenerationException, JsonMappingException, ClientProtocolException, IOException {
        Transfer transfer = new Transfer();
        transfer.setAccountFrom(account1.getId());
        transfer.setAccountTo(account2.getId());
        transfer.setTransferAmount(account1.getBalance());
        
        HttpTest.createPost("http://localhost:8089/transfers")
            .setInputEntity(transfer)
            .expectStatusCode(200)
            .execute();
        
        HttpTest.createGet("http://localhost:8089/transfers/0")
            .expectStatusCode(200)
            .expectOutputEntity(Transfer.class, serverTransfer -> {
                assertThat(serverTransfer).isEqualToComparingOnlyGivenFields(transfer, "accountFrom", "accountTo", "transferAmount");})
            .execute();
    }
    
    @Test
    public void negativeTransfer() throws JsonGenerationException, JsonMappingException, ClientProtocolException, IOException {
        Transfer transfer = new Transfer();
        transfer.setAccountFrom(account1.getId());
        transfer.setAccountTo(account2.getId());
        transfer.setTransferAmount(-10);
        
        HttpTest.createPost("http://localhost:8089/transfers")
            .setInputEntity(transfer)
            .expectStatusCode(400)
            .expectText("Transfer amount can not be negative")
            .execute();
        
        HttpTest.createGet("http://localhost:8089/transfers")
            .setInputEntity(transfer)
            .expectStatusCode(200)
            .expectOutputEntity(Transfer[].class, transfers -> assertThat(transfers).isEmpty())
            .execute();
    }
    
    @Test
    public void getNonExisting() throws ClientProtocolException, IOException {
        HttpTest.createGet("http://localhost:8089/transfers/1")
          .expectStatusCode(404)
          .expectText("Transfer not found")
          .execute();
    }
    
    @Test
    public void insufficientTransfer() throws JsonGenerationException, JsonMappingException, ClientProtocolException, IOException {
        Transfer transfer = new Transfer();
        transfer.setAccountFrom(account1.getId());
        transfer.setAccountTo(account2.getId());
        transfer.setTransferAmount(account1.getBalance() + 1);
        
        HttpTest.createPost("http://localhost:8089/transfers")
            .setInputEntity(transfer)
            .expectStatusCode(400)
            .expectText("Insufficient founds")
            .execute();
        
        HttpTest.createGet("http://localhost:8089/transfers")
            .setInputEntity(transfer)
            .expectStatusCode(200)
            .expectOutputEntity(Transfer[].class, transfers -> assertThat(transfers).isEmpty())
            .execute();
    }
    
    @Test
    public void nonExistingAccountFrom() throws JsonGenerationException, JsonMappingException, ClientProtocolException, IOException {
        Transfer transfer = new Transfer();
        transfer.setAccountFrom(100);
        transfer.setAccountTo(account2.getId());
        transfer.setTransferAmount(account1.getBalance());
        
        HttpTest.createPost("http://localhost:8089/transfers")
            .setInputEntity(transfer)
            .expectStatusCode(400)
            .expectText("Account is missing")
            .execute();
        
        HttpTest.createGet("http://localhost:8089/transfers")
            .setInputEntity(transfer)
            .expectStatusCode(200)
            .expectOutputEntity(Transfer[].class, transfers -> assertThat(transfers).isEmpty())
            .execute();
    }
    
    @Test
    public void nonExistingAccountTo() throws JsonGenerationException, JsonMappingException, ClientProtocolException, IOException {
        Transfer transfer = new Transfer();
        transfer.setAccountFrom(account1.getId());
        transfer.setAccountTo(100);
        transfer.setTransferAmount(account1.getBalance());
        
        HttpTest.createPost("http://localhost:8089/transfers")
            .setInputEntity(transfer)
            .expectStatusCode(400)
            .expectText("Account is missing")
            .execute();
        
        HttpTest.createGet("http://localhost:8089/transfers")
            .setInputEntity(transfer)
            .expectStatusCode(200)
            .expectOutputEntity(Transfer[].class, transfers -> assertThat(transfers).isEmpty())
            .execute();
    }
}
