package com.pm.account.controller;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.jupiter.api.Test;

import com.pm.account.db.dao.UserDAOImpl;
import com.pm.account.model.CurrencyAccount;
import com.pm.account.model.User;
import com.pm.account.model.dao.ex.PersistException;
import com.pm.account.utils.HttpTest;
import com.pm.account.utils.RestTest;

public class UserRestTest extends RestTest {

	@Test
	public void userCrudTest() throws JsonGenerationException, JsonMappingException, IOException, PersistException {
		UserDAOImpl dao = new UserDAOImpl();
		
		assertThat(dao.getAll()).hasSize(3);
		HttpTest.createGet("http://localhost:8089/users")
			.expectStatusCode(200)
			.expectOutputEntity(User[].class, user -> {assertThat(user).hasSize(3);})
			.execute();
		
		User inputUser = new User("Pippin", new CurrencyAccount(1000));
		HttpTest.createPost("http://localhost:8089/users")
			.setInputEntity(inputUser)
			.expectStatusCode(201)
			.expectOutputEntity(User.class, user -> {assertThat(user).isEqualToComparingOnlyGivenFields(inputUser, "name", "account.balance");})
			.execute();
		User dbUser = new UserDAOImpl().get(4).get();
		assertThat(dbUser).isEqualToComparingOnlyGivenFields(inputUser, "name", "account.balance");
		
		assertThat(dao.getAll()).hasSize(4);
		HttpTest.createGet("http://localhost:8089/users")
			.expectStatusCode(200)
			.expectOutputEntity(User[].class, user -> {assertThat(user).hasSize(4);})
			.execute();
		
		HttpTest.createGet("http://localhost:8089/users/4")
			.expectStatusCode(200)
			.expectOutputEntity(User.class, user -> {assertThat(user).isEqualToComparingOnlyGivenFields(inputUser, "name", "account.balance");})
			.execute();
		
		inputUser.setId(4);
		inputUser.setName("Merry");
		HttpTest.createPut("http://localhost:8089/users")
			.setInputEntity(inputUser)
			.expectStatusCode(200)
			.expectOutputEntity(User.class, user -> {assertThat(user).isEqualToComparingOnlyGivenFields(inputUser, "name", "account.balance");})
			.execute();
		dbUser = new UserDAOImpl().get(4).get();
		assertThat(dbUser).isEqualToComparingOnlyGivenFields(inputUser, "name", "account.balance");
		
		HttpTest.createDelete("http://localhost:8089/users/4")
			.expectStatusCode(200)
			.execute();
		assertThat(dao.get(4).isPresent()).isFalse();
	}
	
	@Test
	public void removeNonExisting() throws ClientProtocolException, IOException {
		HttpTest.createDelete("http://localhost:8089/users/100")
			.expectStatusCode(404)
			.expectText("User not found")
			.execute();
	}
	
	@Test
	public void updateNonExisting() throws JsonGenerationException, JsonMappingException, ClientProtocolException, IOException {
		User inputUser = new User(100, "Pippin", new CurrencyAccount(1000));
		HttpTest.createPut("http://localhost:8089/users")
			.setInputEntity(inputUser)
			.expectStatusCode(404)
			.expectText("User not found")
			.execute();
	}
	
	@Test
	public void getNonExisting() throws ClientProtocolException, IOException {
		HttpTest.createGet("http://localhost:8089/users/100")
			.expectStatusCode(404)
			.expectText("User not found")
			.execute();
	}
}
