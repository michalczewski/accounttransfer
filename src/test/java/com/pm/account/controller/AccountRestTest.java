package com.pm.account.controller;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.jupiter.api.Test;

import com.pm.account.db.dao.AccountDAOImpl;
import com.pm.account.model.CurrencyAccount;
import com.pm.account.model.dao.ex.PersistException;
import com.pm.account.utils.HttpTest;
import com.pm.account.utils.RestTest;

public class AccountRestTest extends RestTest {

    @Test
    public void accountGetTest() throws JsonGenerationException, JsonMappingException, IOException, PersistException {
        AccountDAOImpl dao = new AccountDAOImpl();

        assertThat(dao.getAll()).hasSize(3);
        HttpTest.createGet("http://localhost:8089/accounts")
          .expectStatusCode(200)
          .expectOutputEntity(CurrencyAccount[].class, accounts -> {assertThat(accounts).hasSize(3);})
          .execute();

        HttpTest.createGet("http://localhost:8089/accounts/" + account1.getId())
          .expectStatusCode(200)
          .expectOutputEntity(CurrencyAccount.class, account -> {assertThat(account).isEqualToComparingFieldByField(account1);})
          .execute();

        account1.setBalance(0);
        HttpTest.createPut("http://localhost:8089/accounts")
          .setInputEntity(account1)
          .expectStatusCode(200)
          .expectOutputEntity(CurrencyAccount.class, account -> {assertThat(account).isEqualToComparingFieldByField(account1);})
          .execute();
        CurrencyAccount dbAccount = dao.get(account1.getId()).get();
        assertThat(dbAccount).isEqualToComparingFieldByField(account1);
    }

    @Test
    public void updateNonExisting() throws JsonGenerationException, JsonMappingException, ClientProtocolException, IOException {
        CurrencyAccount account = new CurrencyAccount(100, 0);
        HttpTest.createPut("http://localhost:8089/accounts")
          .setInputEntity(account)
          .expectStatusCode(404)
          .expectText("Account not found")
          .execute();
    }

    @Test
    public void getNonExisting() throws ClientProtocolException, IOException {
        HttpTest.createGet("http://localhost:8089/accounts/100")
          .expectStatusCode(404)
          .expectText("Account not found")
          .execute();
    }

    @Test
    public void updateIncorrectBalance() throws JsonGenerationException, JsonMappingException, ClientProtocolException, IOException {
        account1.setBalance(-10);
        HttpTest.createPut("http://localhost:8089/accounts")
          .setInputEntity(account1)
          .expectStatusCode(400)
          .expectText("Balance can not be negative")
          .execute();
    }

    @Test
    public void updateWithoutEntity() throws JsonGenerationException, JsonMappingException, ClientProtocolException, IOException {
        account1.setBalance(-10);
        HttpTest.createPut("http://localhost:8089/accounts")
            .expectStatusCode(415)
            .execute();
    }
}
