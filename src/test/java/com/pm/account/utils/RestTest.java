package com.pm.account.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.BusFactory;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.hsqldb.cmdline.SqlToolError;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import com.pm.account.controller.AccountEndPoint;
import com.pm.account.controller.TransferEndPoint;
import com.pm.account.controller.UserEndPoint;
import com.pm.account.db.DBFactory;
import com.pm.account.model.CurrencyAccount;
import com.pm.account.model.User;

public class RestTest {

	@BeforeAll
	public static void startServer() throws SqlToolError, SQLException, IOException {
		DBFactory.createDB();		
		JAXRSServerFactoryBean factoryBean = new JAXRSServerFactoryBean();
		factoryBean.setResourceProvider(new SingletonResourceProvider(new AccountEndPoint()));
		factoryBean.setResourceProvider(new SingletonResourceProvider(new UserEndPoint()));
		factoryBean.setResourceProvider(new SingletonResourceProvider(new TransferEndPoint()));
		factoryBean.setAddress("http://localhost:8089");
		List<Object> providers = new ArrayList<Object>();
        providers.add(new JacksonJsonProvider());
        factoryBean.setProviders(providers);
		server = factoryBean.create();
	}
	
	@AfterAll
	public static void stopServer() throws SQLException {
		server.stop();
		server.destroy();
		BusFactory.getDefaultBus().shutdown(true);
		try (Connection connection = DBFactory.getConnection()) {
            connection.prepareStatement("drop table users").execute();
            connection.prepareStatement("drop table operations").execute();
            connection.prepareStatement("drop table accounts").execute();
        }
	}
	
	@BeforeEach
	public void prepareDB() throws SqlToolError, SQLException, IOException {
		try (Connection connection = DBFactory.getConnection()) {
			connection.prepareStatement("insert into accounts(account_id, balance) values(1, 10000)").execute();
			connection.prepareStatement("insert into accounts(account_id, balance) values(2, 20000)").execute();
			connection.prepareStatement("insert into accounts(account_id, balance) values(3, 30000)").execute();
			connection.prepareStatement("insert into users(user_id, name, account_id) values(1, 'Frodo', 1)").execute();
			connection.prepareStatement("insert into users(user_id, name, account_id) values(2, 'Sam', 2)").execute();
			connection.prepareStatement("insert into users(user_id, name, account_id) values(3, 'Gandalf', 3)").execute();			
		}
		account1 = new CurrencyAccount(1, 10000);
		account2 = new CurrencyAccount(2, 20000);
		account3 = new CurrencyAccount(3, 30000);
		user1 = new User(1, "Frodo", account1);
		user2 = new User(2, "Sam", account2);
		user3 = new User(3, "Gandalf", account3);
	}
	
	@AfterEach
	public void clearDB() throws SQLException {
		try (Connection connection = DBFactory.getConnection()) {
			connection.prepareStatement("delete from users").execute();
			connection.prepareStatement("delete from operations").execute();
			connection.prepareStatement("delete from accounts").execute();
		}
	}
	
	protected static CurrencyAccount account1;
	protected static CurrencyAccount account2;
	protected static CurrencyAccount account3;
	protected static User user1;
	protected static User user2;
	protected static User user3;
	
	private static Server server;
}
