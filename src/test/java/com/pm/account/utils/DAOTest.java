package com.pm.account.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.hsqldb.cmdline.SqlToolError;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import com.pm.account.db.DBFactory;
import com.pm.account.model.CurrencyAccount;
import com.pm.account.model.User;

public class DAOTest {

	@BeforeAll
	public static void createDB() throws SqlToolError, SQLException, IOException {
		DBFactory.createDB();		
	}
	
	@AfterAll
	public static void removeDB() throws SQLException {
		try (Connection connection = DBFactory.getConnection()) {
			connection.prepareStatement("drop table users").execute();
			connection.prepareStatement("drop table operations").execute();
			connection.prepareStatement("drop table accounts").execute();
		}
	}
	
	@BeforeEach
	public void prepareDB() throws SqlToolError, SQLException, IOException {
		try (Connection connection = DBFactory.getConnection()) {
			connection.prepareStatement("insert into accounts(account_id, balance) values(1, 10000)").execute();
			connection.prepareStatement("insert into accounts(account_id, balance) values(2, 20000)").execute();
			connection.prepareStatement("insert into accounts(account_id, balance) values(3, 30000)").execute();
			connection.prepareStatement("insert into users(user_id, name, account_id) values(1, 'Frodo', 1)").execute();
			connection.prepareStatement("insert into users(user_id, name, account_id) values(2, 'Sam', 2)").execute();
			connection.prepareStatement("insert into users(user_id, name, account_id) values(3, 'Gandalf', 3)").execute();
		}
		account1 = new CurrencyAccount(1, 10000);
		account2 = new CurrencyAccount(2, 20000);
		account3 = new CurrencyAccount(3, 30000);
		user1 = new User(1, "Frodo", account1);
		user2 = new User(2, "Sam", account2);
		user3 = new User(3, "Gandalf", account3);
	}
	
	@AfterEach
	public void clearDB() throws SQLException {
		try (Connection connection = DBFactory.getConnection()) {
			connection.prepareStatement("delete from users").execute();
			connection.prepareStatement("delete from operations").execute();
			connection.prepareStatement("delete from Accounts").execute();
		}
	}
	
	protected static CurrencyAccount account1;
	protected static CurrencyAccount account2;
	protected static CurrencyAccount account3;
	protected static User user1;
	protected static User user2;
	protected static User user3;
}
