package com.pm.account.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Consumer;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class HttpTest {

    public void execute() throws ClientProtocolException, IOException {
        DefaultHttpClient client = new DefaultHttpClient();
        if (entity != null && request instanceof HttpEntityEnclosingRequestBase) {
            ((HttpEntityEnclosingRequestBase)request).setEntity(entity);
        }
        HttpResponse response = client.execute(request);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(statusCode);

        if (outputConsumer != null) {
            outputConsumer.accept(response);
        }
        if (outputText != null) {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()))) {
                assertThat(br.readLine()).isEqualTo(outputText);
            }
        }
        client.getConnectionManager().shutdown();
    }

    private HttpTest(HttpUriRequest request) {
        this.request = request;
    }

    public static HttpTest createGet(String url) {
        return new HttpTest(new HttpGet(url));
    }

    public static HttpTest createPost(String url) {
        return new HttpTest(new HttpPost(url));
    }

    public static HttpTest createPut(String url) {
        return new HttpTest(new HttpPut(url));
    }

    public static HttpTest createDelete(String url) {
        return new HttpTest(new HttpDelete(url));
    }

    public HttpTest setInputEntity(Object entity) throws JsonGenerationException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();		
        String str = mapper.writeValueAsString(entity);
        this.entity = new StringEntity(str);
        this.entity.setContentType("application/json");
        return this;
    }

    public HttpTest expectStatusCode(int statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public <T> HttpTest expectOutputEntity(Class<T> cls, Consumer<T> consumer) {
        outputConsumer = response -> {
            ObjectMapper mapper = new ObjectMapper();	
            T responseEntity;
            try {
                responseEntity = mapper.readValue(response.getEntity().getContent(), cls);
                consumer.accept(responseEntity);
            } catch (IllegalStateException | IOException e) {
                fail(e);
            }
        };
        return this;
    }

    public HttpTest expectText(String text) {
        outputText = text;
        return this;
    }

    private HttpUriRequest request;
    private StringEntity entity;
    private int statusCode;
    private Consumer<HttpResponse> outputConsumer;
    private String outputText;
}
